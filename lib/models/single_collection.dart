class SingleCollection {
  final int id;
  final int timeStamp;
  final int weight;
  final int garbagebin_id;
  final int tour_id;
  final int payment_id;

  SingleCollection(this.id, this.weight, this.timeStamp, this.garbagebin_id,
      this.tour_id, this.payment_id);

  factory SingleCollection.fromJson(Map<String, dynamic> json) {
    return SingleCollection(
      json['id'] as int,
      json['weight'] as int,
      json["timeStamp"] as int,
      json["garbagebin_id"] as int,
      json["tour_id"] as int,
      json["payment_id"] as int,
    );
  }
}

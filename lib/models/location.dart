import 'dart:ffi';

class Location {
  final int id;
  final Float location_x;
  final Float location_y;

  Location(this.id, this.location_x, this.location_y);

factory Location.fromJson(Map<String, dynamic> json) {
    return Location (
      json['id'] as int,
      json['location_x'] as Float,
      json["location_y"] as Float
    );
  }
  
}

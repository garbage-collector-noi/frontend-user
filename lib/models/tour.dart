class Tour {
  final int id;
  final DateTime date;
  final String tourcodes;

  Tour(this.id, this.date, this.tourcodes);

  factory Tour.fromJson(Map<String, dynamic> json) {
    return Tour(
      json['id'] as int,
      json['date'] as DateTime,
      json["tourcodes"] as String
    );
  }

}

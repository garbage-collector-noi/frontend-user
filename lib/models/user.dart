class User {
  final int id;
  final String first_name;
  final String last_name;
  final String email;

  User(this.id, this.first_name, this.last_name, this.email);


  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      json['id'] as int,
      json['first_name'] as String,
      json["last_name"] as String,
      json["email"] as String
    );
  }

}

class Payment {
  final int id;
  final String payment_type;
  final int user_id;

  Payment(this.id, this.payment_type, this.user_id);

factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment (
      json['id'] as int,
      json['payment_type'] as String,
      json["user_id"] as int
    );
  }

}

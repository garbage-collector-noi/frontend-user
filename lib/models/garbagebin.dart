class GarbageBin {
  final int id;
  final int filled_percent;
  final int type_id;
  final int user_id;
  final int content_id;

  GarbageBin(this.id, this.filled_percent, this.type_id, this.user_id,
      this.content_id);

factory GarbageBin.fromJson(Map<String, dynamic> json) {
    return GarbageBin (
      json['id'] as int,
      json['filled_percent'] as int,
      json['type_id'] as int,
      json['user_id'] as int,
      json['content_id'] as int
    );
  }

}

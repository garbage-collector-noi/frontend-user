import 'dart:ffi';
import 'dart:io';

import 'package:http/http.dart' as http;

class BinContent {
  final int id;
  final String name;
  final Float price_kg;

  BinContent(this.id, this.name, this.price_kg);


factory BinContent.fromJson(Map<String, dynamic> json) {
    return BinContent (
      json['id'] as int,
      json['name'] as String,
      json['price_kg'] as Float
    );
  }

}

Future getAll() async{

  final t = await http.get(Uri.parse("https://jsonplaceholder.typicode.com/todos/1"));

  print(t.body);

}


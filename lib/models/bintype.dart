import 'dart:ffi';

import 'dart:io';

class Bintype {
  final int id;
  final String name;

  Bintype(this.id, this.name);

factory Bintype.fromJson(Map<String, dynamic> json) {
    return Bintype(
      json['id'] as int,
      json['name'] as String
    );
  }

}

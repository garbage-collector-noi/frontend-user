class Driver {
  final int id;
  final String name;

  Driver(this.id, this.name);

factory Driver.fromJson(Map<String, dynamic> json) {
    return Driver (
      json['id'] as int,
      json['name'] as String
    );
  }

}

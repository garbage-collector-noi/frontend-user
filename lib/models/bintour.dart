class BinTour {
  final int id;
  final int bin_id;
  final int tour_id;

  BinTour(this.id, this.bin_id, this.tour_id);

factory BinTour.fromJson(Map<String, dynamic> json) {
    return BinTour (
      json['id'] as int,
      json['bin_id'] as int,
      json['tour_id'] as int
    );
  }

}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class BottomBar extends StatelessWidget {
  const BottomBar({ Key? key }) : super(key: key);


  static List<BottomNavigationBarItem> navigation = [
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: "Home",

    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.add_chart),
        label: "Statistics"
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.settings),
        label: "Settings"
    ),
  ];

  String eval_navpress(int){
    switch (int) {
      case 1:
        return "/statistics";
      case 2:
        return "/settings";

      default:
        return "/";
    }

  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: navigation,
      onTap: (int a) {
        Navigator.of(context).pushNamed(eval_navpress(a));
      },
    );
  }
}
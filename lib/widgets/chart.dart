import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:frontend_user/models/single_collection.dart';

class Chart extends StatelessWidget {
  Chart();

  final List<SingleCollection> data = [
    SingleCollection(0, 1, 10, 0, 0, 0),
    SingleCollection(0, 2, 30, 0, 0, 0),
    SingleCollection(0, 3, 50, 0, 0, 0),
    SingleCollection(0, 4, 0, 0, 0, 0),
    SingleCollection(0, 5, 30, 0, 0, 0)
  ];

  @override
  Widget build(BuildContext context) {
    List<charts.Series<SingleCollection, num>> series = [
      charts.Series(
        id: "developers",
        data: data,
        domainFn: (series, _) => series.timeStamp,
        measureFn: (series, _) => series.weight,
      )
    ];

    return Container(
        height: 250,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text("Hallo"),
            charts.LineChart(
              series,
              animate: true,
              behaviors: [new charts.PanAndZoomBehavior()],
            )
          ],
        ));
  }
}

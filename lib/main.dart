import 'package:flutter/material.dart';
import 'package:frontend_user/components/overview.dart';
import 'package:frontend_user/components/settings.dart';
import 'package:frontend_user/components/statistics.dart';
import 'package:frontend_user/widgets/bottom_bar.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'frontend_user',
      initialRoute: '/',
      routes: {
        '/': (context) => Overview(),
        '/statistics': (context) =>  Statistics(),
        '/settings': (context) => Settings(),
        '/price_info': (context) => Settings(),
      },

    ),
  );
}

class App extends StatelessWidget {
  App(this.body);
  final body;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('General'),
      ),
      body: body,
      bottomNavigationBar: BottomBar(),
    );
  }
}

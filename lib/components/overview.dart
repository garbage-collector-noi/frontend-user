
import 'package:flutter/material.dart';
import 'package:frontend_user/widgets/bottom_bar.dart';
import 'package:frontend_user/widgets/chart.dart';

class Overview extends StatelessWidget {
  const Overview({Key? key}) : super(key: key);

  static List<BottomNavigationBarItem> navigation = [
    BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: "Home",
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.add_chart),
        label: "Statistics"
    ),
    BottomNavigationBarItem(
        icon: Icon(Icons.settings),
        label: "Settings"
    ),
  ];

  String eval_navpress(int){
    switch (int) {
      case 1:
        return "/statistics";
      case 2:
        return "/settings";
      default:
        return "/";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomBar(),
      appBar: AppBar(
        title: const Text('Overview'),
      ),
      body: Center(
        child: Chart(),
      ),
    );
  }
}